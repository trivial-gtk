
(asdf:defsystem :gdk
  :depends-on (:iterate :cffi)
  :serial t
  :components
  ((:file "gdk-package")   
   (:file "gdk-types")
   (:file "gdk-funcs")))
