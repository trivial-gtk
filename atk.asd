

(asdf:defsystem :atk
  :depends-on (:iterate :cffi)
  :serial t
  :components
  ((:file "atk-package")
   (:file "atk-types")
   (:file "atk-funcs")))
