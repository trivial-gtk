;;;; gir.asd
(in-package :asdf)


(asdf:defsystem :gir
  :depends-on (:iterate :cffi :s-xml :alexandria :cl-fad)
  :version 0.01
  :serial t
  :components 
  ((:file "package")
   (:file "gir-loader")
   (:file "gir-parser")
   (:file "gir-generator")))