(in-package :gir)

(defun named-xml-element-child (element name)
  (find-if #'(lambda (x)
               (and (s-xml::xml-element-p x)
                    (equal (s-xml::xml-element-name x) name)))
           (s-xml::xml-element-children element)))

(defun is-field-element-p (element)
  (equal  (s-xml:xml-element-name element) 'GIR-CORE::|field|))

(defun is-callback-element-p (element)
  (equal  (s-xml:xml-element-name element) 'GIR-CORE::|callback|))

(defun is-object-record (record)
  (let* ((record-string (string record))
         (record-string-len (length record-string)))
    (or
     (and (> record-string-len 6)
          (string= "OBJECT" (string-upcase (subseq  record-string (- record-string-len 6))))))))

(defun parse-function-types (outs method)
  (let ((return-value
         (named-xml-element-child method 'GIR-CORE:|return-value|))
        (parameters 
         (named-xml-element-child method 'GIR-CORE:|parameters|)))      
    (let ((return-type (first (xml-element-children return-value))))
        (format outs " ~A~%" (resolve-type (s-xml:xml-element-attribute return-type 'GIR-C::|type|))))
    (when (s-xml::xml-element-p parameters)
      (iterate
        (for parameter in (s-xml:xml-element-children parameters))
        (format outs "~&~T( ~A " (s-xml:xml-element-attribute parameter :|name|))
        (format outs " ~A )"
                (resolve-type (xml-element-attribute (car (s-xml:xml-element-children parameter)) 'GIR-C::|type|)))))))

(defun parse-signal-types (outs gsignal)
  (let ((name (s-xml:xml-element-attribute gsignal :|name|)))
    (format outs "~%; ----------------- ~A ----------------~%" name)
    (format outs "(defmacro make-~A (name &rest body) ~%" (lispize name))
    (format outs "~T(defcallback ,name ")
    (let ((return-value (named-xml-element-child gsignal 'GIR-CORE::|return-value|)))
      (let ((return-type (first (xml-element-children return-value))))
        (format outs "~&~T~A" (resolve-type (s-xml:xml-element-attribute return-type 'GIR-C::|type|))))
      (format outs "~T( ")
      (iterate
        (for item in (s-xml:xml-element-children gsignal))
        (when (s-xml::xml-element-p item)
          (case (s-xml:xml-element-name item)
            (GIR-CORE::|parameters|
                       (iterate
                         (for parameter in (s-xml:xml-element-children item))
                         (format outs "( ~A " (s-xml:xml-element-attribute parameter :|name|))
                         (format outs " ~A ) "
                                 (resolve-type (xml-element-attribute (car (s-xml:xml-element-children parameter)) 'GIR-C::|type|))))))))
      (format outs ")"))    
    (format outs "~%~T,@body))~%")))



(defun parse-record (outs record)
  (let ((c-type (s-xml:xml-element-attribute record  'GIR-C::|type|))
        (name (s-xml:xml-element-attribute record :|name|)))
    ;; we punt on object records (I think they are meant to be opaque)
    (add-type name :struct)
    ;; if the struct is actually an object type, we aren't interested (they are supposed to be opaque!?)
    (unless (is-object-record name)
      (let
          ((fields (remove-if-not #'is-field-element-p (s-xml::xml-element-children record)))
           (callbacks (remove-if-not #'is-callback-element-p (s-xml::xml-element-children record))))
        (format outs "~%; ----------------- ~A ----------------~%" name)
        (when (or (not (zerop (length fields))) (not (zerop (length callbacks))))
          (format outs "~%; ----------------- ~A : fields ----------------~%" name)
          (when (not (zerop (length fields)))
            (format outs  "~%(defcstruct ~A ~%" name)
            (iterate
              (for field in fields)
              (format outs "~&~T(~A ~A)"
                      (lispize (s-xml:xml-element-attribute field :|name|))
                      (resolve-type (s-xml:xml-element-attribute 
                                     (named-xml-element-child field 'GIR-CORE:|type|) 
                                     'GIR-C::|type|))))
              (format outs ")~%"))
          (when (not (zerop (length callbacks)))
            (format outs "~%; ----------------- ~A : methods ----------------~%" name)
            (iterate
              (for method in callbacks)
              (let ((name (s-xml:xml-element-attribute method :|name|)))
                (format outs "(defcfun (~S ~A) " name (lispize name))
                (parse-function-types outs  method)
                (format outs ")~%~%")))))))))



(defun parse-function (outs function)
  (let ((c-identifier (s-xml:xml-element-attribute function  'GIR-C::|identifier|))
        (name (s-xml:xml-element-attribute function :|name|)))
    (format outs "~%; ----------------- Function: ~A ---------------- " name)
    (format outs "~%(defcfun (~S ~A)" c-identifier (lispize name))
    (parse-function-types outs function)
    (format outs ")~%")))

(defun parse-constructor (outs constructor)
  (let ((c-identifier (s-xml:xml-element-attribute constructor  'GIR-C::|identifier|))
        (name (s-xml:xml-element-attribute constructor :|name|)))
    (format outs "~%; ----------------- Constructor: ~A ---------------- " (lispize c-identifier))
    (format outs "~%(defcfun (~S ~A) " c-identifier (lispize c-identifier))
    (parse-function-types  outs constructor)
    (format outs ")~%")))

;; callbacks associated with the records are actually methods


(defun parse-enum-members (outs enum)
  (iterate
    (for member in (s-xml:xml-element-children enum))
    (when (eql (s-xml:xml-element-name member) 'GIR-CORE::|member|)
      (format outs "~&~T(:~A ~S)"
              (string-upcase (s-xml:xml-element-attribute member :|name|))
              (parse-integer (s-xml:xml-element-attribute member :|value|))))))

(defun parse-enumeration (outs enum)
  (let ((c-type (s-xml:xml-element-attribute enum  'GIR-C::|type|))
        (name (s-xml:xml-element-attribute enum :|name|)))
    (format outs "~%; ----------------- Enumeration: ~A ----------------~%" (lispize c-type))
    (add-type (string c-type) :unsigned-int)
    (format outs "(defcenum ~A " name)
    (parse-enum-members outs  enum)
    (format outs ")~%")))

(defun parse-bitfield (outs bitfield)
  (let ((c-type (s-xml:xml-element-attribute bitfield 'GIR-C::|type|))
        (name (s-xml:xml-element-attribute bitfield :|name|)))
    (format outs "~%; ----------------- ~A ----------------~%" (lispize c-type))
    (format outs "(defcenum ~A  " name)
    (add-type (string c-type) :unsigned-int)
    (parse-enum-members outs  bitfield)
    (format outs ")~%")))

(defun parse-class (outs gclass)
  (iterate
    (for method in (s-xml:xml-element-children gclass))
    (when (s-xml::xml-element-p method)
      (case (s-xml:xml-element-name method)
        (GIR-CORE::|constructor| (parse-constructor outs method))
        (GIR-CORE::|method|  (parse-function outs  method))
        (GIR-CORE::|callback| (parse-signal-types outs method))))))

(defun parse-boxed (outs boxed)
  (iterate
    (for method in (s-xml:xml-element-children boxed))
    (when (s-xml::xml-element-p method)
      (case (s-xml:xml-element-name method)
        (GIR-CORE::|constructor| (parse-constructor outs method))
        (GIR-CORE::|method|  (parse-function outs method))
        (GIR-CORE::|callback| (parse-signal-types outs method))))))

(defun parse-callback (outs callback)
  (let ((name (s-xml:xml-element-attribute callback :|name|)))
    (add-type name :pointer)
    (parse-signal-types outs callback)))

(defparameter *gir-top-level-type-elements*
  ;; first pass --
  '((GIR-CORE::|bitfield| . parse-bitfield)
    (GIR-CORE::|enumeration| . parse-enumeration)
    (GIR-CORE::|record|  . parse-record)))

(defparameter *gir-top-level-function-elements*
  ;; second pass
  '((GIR-CORE::|callback| . parse-callback)
    (GIR-CORE::|function| . parse-function)
    (GIR-CORE::|class| . parse-class)
    (GIR-GLIB::|boxed| . parse-boxed)))


(defun parse-element (outs elements entry)
  (let ((name (s-xml:xml-element-name entry)))
    (let ((fun (cdr (assoc name elements))))
      (if fun
          (funcall fun outs entry)))))


(defun parse-namespace (outs namespace elements)
  (let ((namespace-name
         (alexandria::make-keyword
          (string-upcase (s-xml:xml-element-attribute namespace :|name|)))))
    ;;   (format outs "(defpackage ~S (:use :common-lisp :cffi :iterate))~%~%" namespace-name)
    (format outs "(in-package ~S)~%" namespace-name)
    (iterate
      (for entry in (xml-element-children namespace))
      (parse-element outs elements entry))))

(defun parse-repository (outs repository elements)
  (iterate
    (for namespace in (s-xml:xml-element-children repository))
    (parse-namespace outs namespace elements)))
