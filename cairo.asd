
(asdf:defsystem :cairo
  :depends-on (:iterate :cffi)
  :serial t
  :components
  ((:file "cairo-package")
   (:file "cairo-types")
   (:file "cairo-funcs")))
