
#-(or win32 mswindows windows darwin)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (cffi:load-foreign-library "libcairo.so")
  (cffi:load-foreign-library "libgthread-2.0.so")
  (cffi:load-foreign-library "libgtk-x11-2.0.so"))

#+darwin
(eval-when (:compile-toplevel :load-toplevel :execute)
  (let ((cffi:*foreign-library-directories*
	 (cons "/opt/local/lib/" cffi:*foreign-library-directories*)))
    (cffi:load-foreign-library "libcairo.dylib")
    (cffi:load-foreign-library "libgthread-2.0.dylib")
    (cffi:load-foreign-library "libgtk-x11-2.0.dylib")))

#+(or win32 mswindows windows)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (cffi:load-foreign-library "libcairo-2.dll")
  (cffi:load-foreign-library "libglib-2.0-0.dll")
  (cffi:load-foreign-library "libgthread-2.0-0.dll")
  (cffi:load-foreign-library "libgobject-2.0-0.dll")
  (cffi:load-foreign-library "libgdk-win32-2.0-0.dll")
  (cffi:load-foreign-library "libgtk-win32-2.0-0.dll")
  (cffi:load-foreign-library "libpangocairo-1.0-0.dll")
  (cffi:load-foreign-library "libpango-1.0-0.dll"))

;; user-visible structures

(cffi:defcstruct cairo_text_extents
  (x_bearing :double)
  (y_bearing :double)
  (width :double)
  (height :double)
  (x_advance :double)
  (y_advance :double))

(cffi:defcstruct cairo_font_extents
  (ascent :double)
  (descent :double)
  (height :double)
  (max_x_advance :double)
  (max_y_advance :double))

(cffi:defcstruct cairo_glyph
  (index :unsigned-int)
  (x :double)
  (y :double))

(cffi:defcstruct cairo_matrix_t
  (xx :double)
  (yx :double)
  (xy :double)
  (yy :double)
  (x0 :double)
  (y0 :double))


;; enums
;; (can't look these up yet, why?)

(cffi:defcenum cairo_format_t
    :argb32 :rgb24 :a8 :a1)

(cffi:defcenum cairo_operator_t
  :clear
  :src :over :in :out :atop
  :dest :dest_over :dest_in :dest_out :dest_atop
  :xor :add :saturate)

(cffi:defcenum cairo_fill_rule_t
    :winding :even_odd)

(cffi:defcenum cairo_line_cap_t
    :butt :round :square)

(cffi:defcenum cairo_line_join_t
    :miter :round :bevel)

(cffi:defcenum cairo_font_slant_t
    :normal :italic :oblique)

(cffi:defcenum cairo_font_weight_t
    :normal :bold)

(cffi:defcenum cairo_status_t
  :success     
  :no_memory  
  :invalid_restore    
  :invalid_pop_group  
  :no_current_point   
  :invalid_matrix     
  :invalid_status     
  :null_pointer       
  :invalid_string     
  :invalid_path_data  
  :read_error         
  :write_error        
  :surface_finished   
  :surface_type_mismatch      
  :pattern_type_mismatch
  :invalid_content
  :invalid_format
  :invalid_visual
  :file_not_found     
  :invalid_dash)

(cffi:defcenum cairo_filter_t
    :fast :good :best :nearest :bilinear :gaussian)

(cffi:defcenum cairo_extend_t
    :none :repeat :reflect)

(cffi:defcenum cairo_content_t
  (:cairo_content_color #x1000)
  (:cairo_content_alpha #x2000)
  (:cairo_content_color_alpha #x3000))

(cffi:defcenum cairo_antialias_t
  :CAIRO_ANTIALIAS_DEFAULT
  :CAIRO_ANTIALIAS_NONE
  :CAIRO_ANTIALIAS_GRAY
  :CAIRO_ANTIALIAS_SUBPIXEL)
