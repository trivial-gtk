
(asdf:defsystem :pango
  :depends-on (:iterate :cffi)
  :serial t
  :components
  ((:file "pango-package")   
   (:file "pango-types")
   (:file "pango-funcs")))
