
;; (asdf:oos 'asdf:load-op 'iterate)
;; (asdf:oos 'asdf:load-op 'cffi)
;; (asdf:oos 'asdf:load-op 's-xml)
;; (asdf:oos 'asdf:load-op 'alexandria)
;; (asdf:oos 'asdf:load-op 'cl-fad)

(defpackage :gir
  (:use :common-lisp :s-xml :iterate)
  (:export "produce-binding"))

(in-package :gir)


(defpackage :gir-core)
(s-xml::register-namespace "http://www.gtk.org/introspection/core/1.0" "" :GIR-CORE)

(defpackage :gir-c)
(s-xml::register-namespace "http://www.gtk.org/introspection/c/1.0" "c" :GIR-C)

(defpackage :gir-glib)
(s-xml::register-namespace "http://www.gtk.org/introspection/glib/1.0" "glib" :GIR-GLIB)
