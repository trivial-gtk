
;; typedef char   gchar;
;; typedef short  gshort;
;; typedef long   glong;
;; typedef int    gint;
;; typedef gint   gboolean;

;; typedef unsigned char   guchar;
;; typedef unsigned short  gushort;
;; typedef unsigned long   gulong;
;; typedef unsigned int    guint;

;; typedef float   gfloat;
;; typedef double  gdouble; 

;; typedef gulong GType
;; typedef guint32 GQuark

;; TO DO 1 - expand enums
;;       2 - resolve simple types, above

(asdf:oos 'asdf:load-op 'iterate) 
(asdf:oos 'asdf:load-op 'alexandria)
(asdf:oos 'asdf:load-op 'cl-fad)

(defpackage :defs
  (:use :cl :iterate))

(in-package :defs)

(defparameter *type-resolver-table* (make-hash-table :test 'equal))

(defun add-type (newtype basetype)
  (setf (gethash newtype *type-resolver-table*) basetype))

(defun is-pointer (type)
  (let ((type-string (string type)))
	(char= (char type-string (1- (length type-string))) #\*)))

(defun is-func-pointer (type)
  (let* ((type-string (string type))
		 (type-string-len (length type-string)))
    (or
     (and (> type-string-len 4)
          (string= "FUNC" (string-upcase (subseq  type-string (- type-string-len 4)))))
     (and (> type-string-len (length "Function"))
          (string= "FUNCTION" (string-upcase (subseq  type-string (- type-string-len (length "Function")))))))))

(defun resolve-type (type)
  (cond 
    ((null type) :void)
    ((or (is-pointer type) (is-func-pointer type))
     :pointer)
    (t
     (let ((result (gethash type *type-resolver-table* nil)))
       (unless result
         ;; offer restarts?
         :pointer)
       (unless (symbolp result)
         (resolve-type result))
       result))))
  
(add-type "gchar" :char)
(add-type "gshort" :short)
(add-type "glong" :long)
(add-type "gint" :int)
(add-type "guint32" :unsigned-int)
(add-type "gboolean" "gint")
(add-type "guchar" :unsigned-char)
(add-type "gushort" :unsigned-short)
(add-type "gulong" :unsigned-long)
(add-type "guint" :unsigned-int)
(add-type "gfloat" :float)
(add-type "gdouble" :double)
(add-type "gpointer" :pointer)
(add-type "none" :void)
(add-type "GType" "gulong") ;; actually its std::size_t - if it isn't gulong we lose :(
(add-type "GQuark" "guint")
(add-type "GdkDestroyNotify" :pointer)
(add-type "GtkDestroyNotify" :pointer)

;; enum not declared
(add-type "GdkModifierType" :unsigned-long)

(add-type "GtkAccelMapForeach" :pointer)
(add-type "GCallback" :pointer)
(add-type "GdkAtom" :pointer)

;; void (*GDestroyNotify) (gpointer data)

;; Specifies the type of function which is called when a data element
;; is destroyed. It is passed the pointer to the data element and
;; should free any memory and resources allocated for it.

(add-type "GDestroyNotify" :pointer)

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

(defmacro once-only ((&rest names) &body body)
  (let ((gensyms (loop for n in names collect (gensym))))
    `(let (,@(loop for g in gensyms collect `(,g (gensym))))
	   `(let (,,@(loop for g in gensyms for n in names collect ``(,,g ,,n)))
		  ,(let (,@(loop for n in names for g in gensyms collect `(,n ,g)))
				,@body)))))

(defun remove-prefix (prefix string)
  (let ((result
		 (if (equalp (subseq string 0 (1- (length prefix))) prefix)
			 (subseq string 0 (1- (length prefix)))
			 string)))
	(if (member (char result 0) '( #\_ #\-))
		(subseq result 1)
		result)))

(defun dequote (l)
  "If the first element of a list is a quote, strip it away"
  (when (list l)
    (if (eql  (first l) 'quote)
        (rest l)
        l)))

(defmacro format-with-properties (property-sym &body properties)
  (mapcar 
   #'(lambda (x)
	   `(format t ,(cadr x) 
				(remove-prefix ,(getf property-sym 'in-module) ,(getf property-sym (car x)))))
   properties))
  
;; defs 

(defparameter *def-table* (make-hash-table :test 'equal))

(defun add-def-type (def-name def-fn)
  (setf (gethash def-name *def-table*) def-fn))

(defun call-def-fn (def-name form)
  (let ((exec-fn (gethash def-name *def-table*)))
    (when exec-fn
      (funcall exec-fn form))))

(defparameter *read-and-discard* nil)
(defparameter *top-level-defs-file* nil)

(defun read-defs-file (fname)
  (progn
	(set-dispatch-macro-character #\# #\t
								  #'(lambda (s c n)
									  (declare (ignore s c n))
									  't))
	(set-dispatch-macro-character #\# #\f
								  #'(lambda (s c n)
									  (declare (ignore s c n))
									  'nil))
	(setf *top-level-defs-file* fname)
	(with-open-file (ins fname) 
      (iterate
        (for item in-stream ins)
        (call-def-fn (first item) item)))))

;; include 

(defun exec-def-include (&rest form)
  (let ((fname 
         (merge-pathnames (cadar form) 
                          (directory-namestring *top-level-defs-file*))))    
    (when (cl-fad::file-exists-p fname)
      (format t ";; including ~A ~%" fname)
      (read-defs-file fname))))

(add-def-type 'INCLUDE 'exec-def-include)

(defun destructure-def-form (form)
  (let* 
      ((real-form (car form))
       (name (cadr real-form))
       (info (cddr real-form))
       (result (make-hash-table :test 'eql)))    
    (setf (gethash 'NAME result) name)
    (format t "Name is ~A~%" name)
    (format t "Destructuring ~A~%" info)
    (iterate
      (for item in  info)      
      (format t "processing item  ~A~%" item)
      (setf (gethash (car  item) result) (cdr item)))
    result))

;; enum 
(defun exec-def-enum (&rest form)
  (let ((properties (destructure-def-form form)))  
    (format t ";; enum ~A (~A) " (gethash 'NAME properties) (gethash 'C-NAME properties))
    (when (gethash 'C-NAME properties)
      (add-type (gethash 'C-NAME properties) :unsigned-long))))
 
(add-def-type 'DEFINE-ENUM 'exec-def-enum)

;;		  ,(mapcar #'(lambda (x) `(format *debug-io* "Param : ~S~%" ,(car x))) params)))


;;   (with-gensyms (boxed-props)
;; 	(setf (symbol-plist ,boxed-props) ',@params)
;; 	(format-with-properties 
;; 	 ,boxed-props
;; 	 (gtype-id "(defconstant +~A+~%")
;; 	 (copy-func "(cffi:defcfun ~A :pointer (:pointer fresh-copy))")
;; 	 (free-func "(cffi:defcfun ~A :void (:pointer this))")))))


;; flags

;; (defmacro define-flags (name &rest params)
;;   `(with-gtk-def gtk-def
;; 	 ("flags" ',name ,params)
;;      (let* ((gtk-params (cddddr gtk-def))
;;             (flag-values (find-if #'(lambda (x) (equalp (cadr x) "VALUES"))
;;                                   (cdr gtk-params)))
;;             (c-name (find-if #'(lambda (x) (equalp (cadr x) "C-NAME"))
;;                              (cdr gtk-params))))
;;        ;; to do -- define a bunch of constants for each flag value 
;;        (add-type (cadddr c-name) :unsigned-long))))

(defun exec-def-flags (&rest form)
  (let ((properties (destructure-def-form form)))  
    (format t ";; flags ~A (~A) " (gethash 'NAME properties) (gethash 'C-NAME properties))
    (when (gethash 'C-NAME properties)
      (add-type (gethash 'C-NAME properties) :unsigned-long))))
 
(add-def-type 'DEFINE-FLAGS 'exec-def-flags)

;; probably the silliest name ..
(defmacro def-def-exec  ((name params) &body forms)
  `(add-def-type ',name 
                 (lambda (&rest ,params)
                   ,@forms)))

(def-def-exec (DEFINE-OBJECT params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; object ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

(def-def-exec (DEFINE-INTERFACE params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; interface ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

(def-def-exec (DEFINE-BOXED params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; boxed ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

(def-def-exec (DEFINE-VIRTUAL params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; virtual ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

(def-def-exec (DEFINE-TYPE params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; type ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

(def-def-exec (DEFINE-STRUCT params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; struct ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

(def-def-exec (DEFINE-TYPEDEF params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; typedef ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))


(def-def-exec (DEFINE-POINTER params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; pointer ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

;;(TYPE function NAME ASSISTANT_NEW PARAMS (PARAM C-NAME VALUE gtk_assistant_new) (PARAM IS-CONSTRUCTOR-OF VALUE GtkAssistant) (PARAM RETURN-TYPE VALUE GtkWidget*))

;;(TYPE function NAME LINK_BUTTON_GET_TYPE PARAMS (PARAM C-NAME VALUE gtk_link_button_get_type) (PARAM RETURN-TYPE VALUE GType))

;;(TYPE function NAME PAPER_SIZE_NEW_FROM_PPD PARAMS (PARAM C-NAME VALUE gtk_paper_size_new_from_ppd) (PARAM RETURN-TYPE VALUE GtkPaperSize*) (PARAM PARAMETERS VALUE (const-gchar* ppd_name) (const-gchar* ppd_display_name) (gdouble width) (gdouble height)))

;; (defmacro define-function (name &rest params)
;;   `(with-gtk-def gtk-def ("function" ',name ,params)
;; 	 (let* ((gtk-params (cddddr gtk-def))
;; 			(return-type (find-if #'(lambda (x) (equalp (cadr x) "RETURN-TYPE"))
;; 							 (cdr gtk-params)))			
;; 			(c-name (find-if #'(lambda (x) (equalp (cadr x) "C-NAME"))
;; 							 (cdr gtk-params)))
;; 			(fn-parameters (find-if #'(lambda (x) (equalp (cadr x) "PARAMETERS"))
;; 							 (cdr gtk-params))))
;; 	   (format *debug-io* "~%~%(defcfun ~A ~A" (cadddr c-name) (resolve-type (cadddr return-type)))
;; 	   (when fn-parameters 
;; 		 (loop 
;; 			for fn-parameter in (cdddr fn-parameters)
;; 			do (format *debug-io* "~&~T( ~A ~A )" (cadr fn-parameter) (resolve-type (car fn-parameter)))))
;; 	   (format *debug-io* ")~%"))))


(def-def-exec (DEFINE-FUNCTION params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; function ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))


;; (defmacro define-method (name &rest params)
;;   `(with-gtk-def gtk-def ("method" ',name ,params)
;; 	 (let* ((gtk-params (cddddr gtk-def))			
;; 			(of-object (find-if #'(lambda (x) (equalp (cadr x) "OF-OBJECT"))
;; 							 (cdr gtk-params)))
;; 			(return-type (find-if #'(lambda (x) (equalp (cadr x) "RETURN-TYPE"))
;; 							 (cdr gtk-params)))			
;; 			(c-name (find-if #'(lambda (x) (equalp (cadr x) "C-NAME"))
;; 							 (cdr gtk-params)))
;; 			(fn-parameters (find-if #'(lambda (x) (equalp (cadr x) "PARAMETERS"))
;; 							 (cdr gtk-params))))
;; 	   (format *debug-io* "~%~%(defcfun ~A ~A" (cadddr c-name) (resolve-type (cadddr return-type)))
;; 	   ;; to do -- transforn name from "GtkAccelGroup" style to "gtk-accel-group"
;; 	   (format *debug-io* "~&~T(~A ~S)" (cadddr of-object) :pointer)
;; 	   (when fn-parameters 
;; 		 (loop 
;; 			for fn-parameter in (cdddr fn-parameters)
;; 			do (format *debug-io* "~&~T( ~A ~A )" (cadr fn-parameter) (resolve-type  (car fn-parameter)))))
;; 	   (format *debug-io* ")~%"))))

(def-def-exec (DEFINE-METHOD params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; method ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

(defparameter HAVE_GTK_2_12 t)

(def-def-exec (DEFINE-IFDEF params) 
  (destructuring-bind ((name symb &rest forms)) params
    (when (ignore-errors (symbol-value (find-symbol (string symb))))  
      (iterate
        (for form in forms)
        (call-def-fn (first form) form)))))

(def-def-exec (DEFINE-IFNDEF params) 
  (destructuring-bind ((name symb &rest forms)) params
    (unless (ignore-errors (symbol-value (find-symbol (string symb))))  
      (iterate
        (for form in forms)
        (call-def-fn (first form) form))))



(def-def-exec (DEFINE-PROPERTY params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; property ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

;; (defmacro define-property (name &rest params)
;;   `(with-gtk-def gtk-def ( "property" ',name ,params )
;; 				 (format *debug-io* "~A" gtk-def)))

(def-def-exec (DEFINE-SIGNAL params) 
  (let ((properties (destructure-def-form params)))
    (format t ";; signal ~A ~A " (gethash 'NAME properties)
            (gethash 'C-NAME properties))))

;; (defmacro define-signal (name &rest params)
;;   `(with-gtk-def gtk-def ( "signal" ',name ,params )
;; 				 (format *debug-io* "~A" gtk-def)))





(read-defs-file "/usr/share/pygtk/2.0/defs/gtk.defs")

;; (define-boxed PaperSize
;;   (in-module "Gtk")
;;   (c-name "GtkPaperSize")
;;   (gtype-id "GTK_TYPE_PAPER_SIZE")
;; )

