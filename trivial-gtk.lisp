
(defpackage :trivial-gtk
  (:use :cl :cffi))

(in-package :trivial-gtk)

;;;
;;; System
;;;

;; shamelessly stolen from CLOCC
(defun getenv (var)
  "Return the value of the environment variable."
  #+allegro (sys::getenv (string var))
  #+clisp (ext:getenv (string var))
  #+(or cmu scl)
  (cdr (assoc (string var) ext:*environment-list* :test #'equalp
              :key #'string))
  #+gcl (si:getenv (string var))
  #+lispworks (lw:environment-variable (string var))
  #+lucid (lcl:environment-variable (string var))
  #+mcl (ccl::getenv var)
  #+sbcl (sb-ext:posix-getenv var)
  #-(or allegro clisp cmu gcl lispworks lucid mcl sbcl scl)
  (error 'not-implemented :proc (list 'getenv var)))

(defun (setf getenv) (val var)
  "Set an environment variable."
  #+allegro (setf (sys::getenv (string var)) (string val))
  #+clisp (setf (ext:getenv (string var)) (string val))
  #+(or cmu scl)
  (let ((cell (assoc (string var) ext:*environment-list* :test #'equalp
                     :key #'string)))
    (if cell
        (setf (cdr cell) (string val))
        (push (cons (intern (string var) "KEYWORD") (string val))
              ext:*environment-list*)))
  #+gcl (si:setenv (string var) (string val))
  #+lispworks (setf (lw:environment-variable (string var)) (string val))
  #+lucid (setf (lcl:environment-variable (string var)) (string val))
  #-(or allegro clisp cmu gcl lispworks lucid scl)
  (error 'not-implemented :proc (list '(setf getenv) var)))

(defparameter *gtk-location* #P"C:\\Program Files (x86)\\GIMP-2.0\\bin\\"))


#-(or win32 mswindows windows darwin)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (cffi:load-foreign-library "libglib-2.0.so")
  (cffi:load-foreign-library "libgmodule-2.0.so")
  (cffi:load-foreign-library "libgobject-2.0.so")
  (cffi:load-foreign-library "libcario.so")
  (cffi:load-foreign-library "libpango-1.0.so")
  (cffi:load-foreign-library "libpangocairo-1.0.so")
  (cffi:load-foreign-library "libgdk_pixbuf-2.0.so")
  (cffi:load-foreign-library "libgthread-2.0.so")
  (cffi:load-foreign-library "libatk-1.0.so")
  (cffi:load-foreign-library "libgdk-x11-2.0.so"))

#+(or win32 windows)
(push *gtk-location* cffi:*foreign-library-directories*)

#+(or win32 windows)
(eval-when (:compile-toplevel :load-toplevel :execute)  
  (cffi:load-foreign-library "intl.dll")
  (cffi:load-foreign-library "zlib1.dll")
  (cffi:load-foreign-library "libpng13.dll")
  (cffi:load-foreign-library "libglib-2.0-0.dll")
  (cffi:load-foreign-library "libgmodule-2.0-0.dll")
  (cffi:load-foreign-library "libgobject-2.0-0.dll")
  (cffi:load-foreign-library "libcairo-2.dll")
  (cffi:load-foreign-library "libpango-1.0-0.dll")
  (cffi:load-foreign-library "libpangowin32-1.0-0.dll")
  (cffi:load-foreign-library "libpangocairo-1.0-0.dll")
  (cffi:load-foreign-library "libgdk_pixbuf-2.0-0.dll")
  (cffi:load-foreign-library "libgthread-2.0-0.dll")
  (cffi:load-foreign-library "libatk-1.0-0.dll")
  (cffi:load-foreign-library "libgdk-win32-2.0-0.dll"))

