
(asdf:defsystem :gtk
  :depends-on (:iterate :cffi)
  :serial t
  :components
  ((:file "gtk-package")   
   (:file "gtk-types")
   (:file "gtk-funcs")))

