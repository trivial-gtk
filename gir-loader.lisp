;; Copyright (c) 2008 John Connors (johnc@yagc.ndo.remove.this.please.co.uk).
;;
;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"), to
;; deal in the Software without restriction, including without limitation the
;; rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
;; sell copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(in-package :gir)

(defun lispize (fn-name)
  "Turn a c-gtk name into a lisp one"
  (intern (substitute #\- #\_
                      (if (symbolp fn-name)
                        (string  fn-name)
                        fn-name))))

(defparameter *type-resolver-table* (make-hash-table :test 'equal))

(defun add-type (newtype basetype)
  (setf (gethash newtype *type-resolver-table*) basetype))

(defun is-pointer (type)
  (let ((type-string (string type)))
	(char= (char type-string (1- (length type-string))) #\*)))

(defun dump-resolver-table ()
	(iterate
	  (for (key val) in-hashtable  *type-resolver-table*)
	  (format t "Type ~A~TResolution ~S~%" key val)))

(defun is-func-pointer (type)
  (let* ((type-string (string type))
		 (type-string-len (length type-string)))
    (or
     (and (> type-string-len 4)
          (string= "FUNC" (string-upcase (subseq  type-string (- type-string-len 4)))))
     (and (> type-string-len (length "Function"))
          (string= "FUNCTION" (string-upcase (subseq  type-string (- type-string-len (length "Function")))))))))

(defparameter *unknown-types* nil)

(defun resolve-type (type)
  (cond 
    ((null type) ":void")
    ((is-pointer type) ":pointer")
	((is-func-pointer type) ":pointer")
    (t
     (let ((result (gethash type *type-resolver-table* nil)))
       (if (or (null result) (symbolp result))
           (if (and  result (not (eql result :struct)))
               (format nil ":~A" result)
             (let ((typestring (string (intern type))))
               (unless (eql result :struct)
				   (pushnew typestring *unknown-types*  :test 'equal)
                   (format nil ":pointer")
                   ;; (format t ";; ~S possibly unknown~%"  typestring)
				   )					
               (if (eql result :struct)
                   ;; if the type is a structure type, its probably composed into an object, return it
                   (format nil "~A"
                           typestring)
                   ;; otherwise just default to a pointer
                   (format nil ":pointer"))))
            (resolve-type result))))))
  
;; instead of this, why not use defctype?

(add-type "gchar" :char)
(add-type "gint8" :int8)
(add-type "guint8" :uint8)
(add-type "gshort" :short)
(add-type "gint16" :int16)
(add-type "guint16" :uint16)
(add-type "glong" :long)
(add-type "gint" :int)
(add-type "int" :int)
(add-type "guint32" :uint32)
(add-type "gint32" :int32)
(add-type "gint64" :int64)
(add-type "guint64" :uint64)
(add-type "gboolean" :int)
(add-type "unsigned" :unsigned-int)
(add-type "guchar" :unsigned-char)
(add-type "gushort" :unsigned-short)
(add-type "gulong" :unsigned-long)
(add-type "guint" :unsigned-int)
(add-type "gfloat" :float)
(add-type "float" :float)
(add-type "gdouble" :double)
(add-type "double" :double)
(add-type "gpointer" :pointer)
(add-type "none" :void)
(add-type "void" :void)
(add-type "GType" :uint32) ;; might be  a problem on 64-bit platforms
(add-type "GQuark" :uint32)
(add-type "GdkAtom" :pointer)
(add-type "Atom" :pointer)
(add-type "GDestroyNotify" :pointer)
(add-type "GCallback" :pointer)
(add-type "gconstpointer" :pointer)

;; X Types
(add-type "XID" :uint32)   
(add-type "Mask" :uint32)   
(add-type "Atom" :uint32)   
(add-type "VisualID" :uint32)   
(add-type "Time" :uint32)    
(add-type "Window" :uint32)   
(add-type "Drawable" :uint32)   
(add-type "Font" :uint32)    
(add-type "Pixmap" :uint32)   
(add-type "Cursor" :uint32)    
(add-type "Colormap" :uint32)   
(add-type "GContext" :uint32)    
(add-type "KeySym" :uint32)    

;; missing enums
(add-type "cairo_svg_version_t" :uint)
(add-type "cairo_ps_level_t" :uint)
(add-type "cairo_filter_t" :uint)
(add-type "cairo_extend_t" :uint)
(add-type "cairo_pattern_type_t" :uint)
(add-type "cairo_format_t" :uint)
(add-type "cairo_surface_type_t" :uint)
(add-type "cairo_font_weight_t" :uint)
(add-type "cairo_font_slant_t" :uint)
(add-type "cairo_hint_metrics_t" :uint)
(add-type "cairo_hint_style_t" :uint)
(add-type "cairo_subpixel_order_t" :uint)
(add-type "cairo_bool_t" :uint)
(add-type "cairo_line_join_t" :uint)
(add-type "cairo_line_cap_t" :uint)
(add-type "cairo_fill_rule_t" :uint)
(add-type "cairo_antialias_t" :uint)
(add-type "cairo_operator_t" :uint)
(add-type "cairo_content_t" :uint)
(add-type "cairo_status_t" :uint)



(defun parse-gir (gir-in)
  (s-xml:parse-xml gir-in :output-type :xml-struct))

(defun parse-gir-file (pathname)
  (with-open-file (gir-in pathname)
    (parse-gir gir-in)))

(defparameter *repository-files* '("pango-1.0.gir"
                                   "cairo.gir"
								   "atk.gir"
                                   "gdk-x11-2.0.gir"
                                   "gtk-x11-2.0.gir"))


(defparameter *gir-files* nil)

(defun repository-name (filename)
  "Given the repository filname, pick out the lib name."
  (let ((firstdot (position #\. filename))
        (firstdash (position #\- filename)))
    (if firstdash
        (subseq filename 0 firstdash)
        (if firstdot
            (subseq filename 0 firstdot)
            filename))))

(defun load-gir-files ()
  (setf *gir-files*
        (iterate 
         (for repository-file in *repository-files*)
         (collect (parse-gir-file  (merge-pathnames (cl-fad::pathname-as-file (concatenate 'string "gir-repository/gir/" repository-file))))))))


