
(in-package :gir)

;; actually generates the binding

(defun produce-binding ()
  (load-gir-files)
  (iterate
   (for gir in *gir-files*)
   (for repository in *repository-files*)
   (with-open-file (outs   
                    (merge-pathnames 
                     (cl-fad::pathname-as-file  (format nil "~A-types.lisp" (repository-name repository)))) 
                    :direction :output 
                    :if-exists :supersede)
     (parse-repository outs gir *gir-top-level-type-elements*)))
  (iterate
    (for gir in *gir-files*)
    (for repository in *repository-files*)
    (with-open-file (outs 
                     (merge-pathnames 
                      (cl-fad::pathname-as-file  (format nil "~A-funcs.lisp" (repository-name repository)))) 
                     :direction :output 
                     :if-exists :supersede)
      (parse-repository outs gir *gir-top-level-function-elements*)))
  (with-open-file (outs (merge-pathnames
                         (cl-fad::pathname-as-file (format nil "missing-types.lisp")))
                        :direction :output
                        :if-exists :supersede)    
    (format outs ";; the following types need to be defined in order to use this binding~%")
    (iterate
      (for unknown in *unknown-types*)
      (format outs ";; ~T~A~%" unknown))
    (format outs ";; thank you for your attention!~%")))

